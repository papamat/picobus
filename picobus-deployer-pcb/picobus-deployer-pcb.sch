EESchema Schematic File Version 4
LIBS:picobus-deployer-pcb-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Picobus Deployer Board"
Date "2019-11-15"
Rev "1.0"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x01 J6
U 1 1 5DC00368
P 9450 5300
F 0 "J6" H 9530 5342 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 9530 5251 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 9450 5300 50  0001 C CNN
F 3 "~" H 9450 5300 50  0001 C CNN
	1    9450 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J7
U 1 1 5DC00BF8
P 9450 5500
F 0 "J7" H 9530 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 9530 5451 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 9450 5500 50  0001 C CNN
F 3 "~" H 9450 5500 50  0001 C CNN
	1    9450 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J5
U 1 1 5DC00D40
P 8050 5400
F 0 "J5" H 7968 5175 50  0000 C CNN
F 1 "Screw_Terminal_01x01" H 7968 5266 50  0000 C CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8050 5400 50  0001 C CNN
F 3 "~" H 8050 5400 50  0001 C CNN
	1    8050 5400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J8
U 1 1 5DC00F6A
P 5050 5000
F 0 "J8" H 5130 5042 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 4951 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 5000 50  0001 C CNN
F 3 "~" H 5050 5000 50  0001 C CNN
	1    5050 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J9
U 1 1 5DC0120D
P 5050 5350
F 0 "J9" H 5130 5392 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 5301 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 5350 50  0001 C CNN
F 3 "~" H 5050 5350 50  0001 C CNN
	1    5050 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J10
U 1 1 5DC02B11
P 5050 5700
F 0 "J10" H 5130 5742 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 5651 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 5700 50  0001 C CNN
F 3 "~" H 5050 5700 50  0001 C CNN
	1    5050 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J11
U 1 1 5DC02B17
P 5050 6050
F 0 "J11" H 5130 6092 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 6001 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 6050 50  0001 C CNN
F 3 "~" H 5050 6050 50  0001 C CNN
	1    5050 6050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J12
U 1 1 5DC02B1D
P 5050 6400
F 0 "J12" H 5130 6442 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 6351 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 6400 50  0001 C CNN
F 3 "~" H 5050 6400 50  0001 C CNN
	1    5050 6400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J13
U 1 1 5DC02B23
P 5050 6750
F 0 "J13" H 5130 6792 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5130 6701 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 5050 6750 50  0001 C CNN
F 3 "~" H 5050 6750 50  0001 C CNN
	1    5050 6750
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5DC02FFF
P 8800 5400
F 0 "SW1" H 8800 5685 50  0000 C CNN
F 1 "SW_SPDT" H 8800 5594 50  0000 C CNN
F 2 "lsf-kicad-lib:D2F-L" H 8800 5400 50  0001 C CNN
F 3 "~" H 8800 5400 50  0001 C CNN
F 4 "D2F-L" H 8800 5400 50  0001 C CNN "MFP"
F 5 "Omron" H 8800 5400 50  0001 C CNN "MFN"
	1    8800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 5400 8600 5400
Wire Wire Line
	9000 5300 9250 5300
Wire Wire Line
	9000 5500 9250 5500
Text Label 8350 5400 0    50   ~ 0
GND
Text Label 9050 5300 0    50   ~ 0
DET_1
Text Label 9050 5500 0    50   ~ 0
DET_2
Wire Wire Line
	4850 5000 4550 5000
Wire Wire Line
	4550 5000 4550 5350
Wire Wire Line
	4550 5700 4050 5700
Wire Wire Line
	4850 5350 4550 5350
Connection ~ 4550 5350
Wire Wire Line
	4550 5350 4550 5700
Wire Wire Line
	4850 5700 4550 5700
Connection ~ 4550 5700
Text Label 4050 5700 0    50   ~ 0
V_DEPLOY
Wire Wire Line
	4850 6050 4550 6050
Wire Wire Line
	4550 6050 4550 6400
Wire Wire Line
	4550 6750 4050 6750
Wire Wire Line
	4850 6750 4550 6750
Connection ~ 4550 6750
Wire Wire Line
	4850 6400 4550 6400
Connection ~ 4550 6400
Wire Wire Line
	4550 6400 4550 6750
Text Label 4050 6750 0    50   ~ 0
GND_D
$Comp
L Connector:Screw_Terminal_01x01 J2
U 1 1 5DC17B73
P 2500 5600
F 0 "J2" H 2580 5642 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 2580 5551 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 2500 5600 50  0001 C CNN
F 3 "~" H 2500 5600 50  0001 C CNN
	1    2500 5600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J1
U 1 1 5DC17FF5
P 2500 5250
F 0 "J1" H 2580 5292 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 2580 5201 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 2500 5250 50  0001 C CNN
F 3 "~" H 2500 5250 50  0001 C CNN
	1    2500 5250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 5250 3150 5250
Wire Wire Line
	2700 5600 3150 5600
Text Label 3150 5250 2    50   ~ 0
V_BURN
Text Label 3150 5600 2    50   ~ 0
GND_D
$Comp
L Connector:Screw_Terminal_01x01 J4
U 1 1 5DC1C079
P 2500 6750
F 0 "J4" H 2580 6792 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 2580 6701 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 2500 6750 50  0001 C CNN
F 3 "~" H 2500 6750 50  0001 C CNN
	1    2500 6750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J3
U 1 1 5DC1C07F
P 2500 6300
F 0 "J3" H 2580 6342 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 2580 6251 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 2500 6300 50  0001 C CNN
F 3 "~" H 2500 6300 50  0001 C CNN
	1    2500 6300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 6300 3150 6300
Wire Wire Line
	2700 6750 3150 6750
Text Label 3150 6300 2    50   ~ 0
V_BURN
Text Label 3150 6750 2    50   ~ 0
GND_D
$Comp
L Device:C C1
U 1 1 5DCEB5BB
P 2900 2450
F 0 "C1" H 3015 2496 50  0000 L CNN
F 1 "4.7u" H 3015 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2938 2300 50  0001 C CNN
F 3 "~" H 2900 2450 50  0001 C CNN
F 4 "C2012X5R1H475K125AB" H 2900 2450 50  0001 C CNN "MFP"
F 5 "TDK" H 2900 2450 50  0001 C CNN "MFN"
	1    2900 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5DCEC076
P 3300 2450
F 0 "C2" H 3415 2496 50  0000 L CNN
F 1 "100n" H 3415 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3338 2300 50  0001 C CNN
F 3 "~" H 3300 2450 50  0001 C CNN
F 4 "C0805C104M5RACTU" H 3300 2450 50  0001 C CNN "MFP"
F 5 "Kemet" H 3300 2450 50  0001 C CNN "MFN"
	1    3300 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2300 2900 1700
Wire Wire Line
	2900 1700 3300 1700
Wire Wire Line
	3300 2300 3300 1700
Connection ~ 3300 1700
Wire Wire Line
	4950 2000 4950 1700
NoConn ~ 4450 2900
Wire Wire Line
	2900 2600 2900 2950
Wire Wire Line
	2900 2950 3300 2950
Wire Wire Line
	3300 2950 3300 2600
Wire Wire Line
	3300 2950 3300 3400
Connection ~ 3300 2950
Text Label 3300 3400 0    50   ~ 0
GND_D
Wire Wire Line
	4950 3400 4950 3650
Wire Wire Line
	4950 3650 5050 3650
Wire Wire Line
	5350 3650 5350 3850
Wire Wire Line
	5350 3650 5350 3400
Connection ~ 5350 3650
Wire Wire Line
	5250 3400 5250 3650
Connection ~ 5250 3650
Wire Wire Line
	5250 3650 5350 3650
Wire Wire Line
	5050 3400 5050 3650
Connection ~ 5050 3650
Wire Wire Line
	5050 3650 5250 3650
Text Label 5350 3850 0    50   ~ 0
GND_D
Wire Wire Line
	2900 1700 2500 1700
Connection ~ 2900 1700
Text Label 1750 1700 0    50   ~ 0
V_DEPLOY
$Comp
L lsf-kicad:NTMFS5C673NL Q1
U 1 1 5DD0E484
P 6350 2600
F 0 "Q1" H 6556 2646 50  0000 L CNN
F 1 "NTMFS5C673NL" H 6550 2750 50  0000 L CNN
F 2 "lsf-kicad-lib:SO-8FL" H 6550 2700 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NTMFS5C673NL-D.PDF" H 6350 2600 50  0001 C CNN
F 4 "NTMFS5C673NLT1G" H 6350 2600 50  0001 C CNN "MFP"
F 5 "ON Semiconductor" H 6350 2600 50  0001 C CNN "MFN"
	1    6350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1700 6450 1700
Wire Wire Line
	6450 1700 6450 2400
Connection ~ 4950 1700
Wire Wire Line
	5850 2600 6150 2600
Wire Wire Line
	6450 2800 5850 2800
Connection ~ 6450 2800
$Comp
L Device:L L1
U 1 1 5DD16388
P 6800 2800
F 0 "L1" V 6700 2800 50  0000 C CNN
F 1 "4.7u" V 6900 2800 50  0000 C CNN
F 2 "lsf-kicad-lib:SRP1270" H 6800 2800 50  0001 C CNN
F 3 "~" H 6800 2800 50  0001 C CNN
F 4 "SRP1270-4R7M" H 6800 2800 50  0001 C CNN "MFP"
F 5 "Bourns" H 6800 2800 50  0001 C CNN "MFN"
	1    6800 2800
	0    -1   -1   0   
$EndComp
Text Label 9250 2800 2    50   ~ 0
V_BURN
$Comp
L Device:C C6
U 1 1 5DD1EF93
P 7350 2950
F 0 "C6" H 7465 2996 50  0000 L CNN
F 1 "100p" H 7465 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7388 2800 50  0001 C CNN
F 3 "~" H 7350 2950 50  0001 C CNN
F 4 "C0805C101K5GACTU" H 7350 2950 50  0001 C CNN "MFP"
F 5 "Kemet" H 7350 2950 50  0001 C CNN "MFN"
	1    7350 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5DD1F532
P 7700 3550
F 0 "R4" H 7770 3596 50  0000 L CNN
F 1 "10k" H 7770 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7630 3550 50  0001 C CNN
F 3 "~" H 7700 3550 50  0001 C CNN
F 4 "CRCW080510K0FKEA" H 7700 3550 50  0001 C CNN "MFP"
F 5 "Vishay-Dale" H 7700 3550 50  0001 C CNN "MFN"
	1    7700 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5DD1FA13
P 7700 2950
F 0 "R3" H 7770 2996 50  0000 L CNN
F 1 "191k" H 7770 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7630 2950 50  0001 C CNN
F 3 "~" H 7700 2950 50  0001 C CNN
F 4 "ERJ-6ENF1913V" H 7700 2950 50  0001 C CNN "MFP"
F 5 "Panasonic" H 7700 2950 50  0001 C CNN "MFN"
	1    7700 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2800 6650 2800
Connection ~ 7350 2800
Wire Wire Line
	7350 2800 7700 2800
Connection ~ 7700 2800
Wire Wire Line
	7350 3100 7350 3300
Wire Wire Line
	4450 4000 4450 3000
Wire Wire Line
	7350 3300 7700 3300
Wire Wire Line
	7700 3300 7700 3100
Connection ~ 7350 3300
Wire Wire Line
	7350 3300 7350 4000
Wire Wire Line
	7700 3300 7700 3400
Connection ~ 7700 3300
Wire Wire Line
	7700 3700 7700 3800
Text Label 9250 3800 2    50   ~ 0
GND_D
Text Label 5850 2800 0    50   ~ 0
SW
Text Label 4000 1900 3    50   ~ 0
SW
$Comp
L Device:C C4
U 1 1 5DD3F296
P 4150 2300
F 0 "C4" V 3898 2300 50  0000 C CNN
F 1 "470n" V 3989 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4188 2150 50  0001 C CNN
F 3 "~" H 4150 2300 50  0001 C CNN
F 4 "GRM21BR71H474KA88L" H 4150 2300 50  0001 C CNN "MFP"
F 5 "Murata" H 4150 2300 50  0001 C CNN "MFN"
	1    4150 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 1900 4000 2300
$Comp
L Device:R R2
U 1 1 5DD45345
P 4150 2700
F 0 "R2" V 4150 2950 50  0000 C CNN
F 1 "3.4k" V 4150 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4080 2700 50  0001 C CNN
F 3 "~" H 4150 2700 50  0001 C CNN
F 4 "ERJ-6ENF3401V" H 4150 2700 50  0001 C CNN "MFP"
F 5 "Panasonic" H 4150 2700 50  0001 C CNN "MFN"
	1    4150 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5DCEB109
P 3750 2150
F 0 "R1" V 3650 2150 50  0000 C CNN
F 1 "187k" V 3750 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 2150 50  0001 C CNN
F 3 "~" H 3750 2150 50  0001 C CNN
F 4 "CRCW0805187KFKEA" H 3750 2150 50  0001 C CNN "MFP"
F 5 "Vishay-Dale" H 3750 2150 50  0001 C CNN "MFN"
	1    3750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1700 3750 1700
$Comp
L lsf-kicad:LM3150MH U1
U 1 1 5DCE9CF2
P 5150 2700
F 0 "U1" H 5750 2250 50  0000 C CNN
F 1 "LM3150MH" H 5150 2700 50  0000 C CNN
F 2 "lsf-kicad-lib:HTSSOP-14" H 5150 2700 50  0001 L BNN
F 3 "http://www.ti.com/lit/ds/snvs561g/snvs561g.pdf" H 5150 2700 50  0001 L BNN
F 4 "LM3150MH/NOPB" H 5150 2700 50  0001 C CNN "MFP"
F 5 "Texas Instruments" H 5150 2700 50  0001 C CNN "MFN"
	1    5150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2300 4350 2300
Wire Wire Line
	4350 2300 4350 2500
Wire Wire Line
	4350 2500 4450 2500
Wire Wire Line
	4450 2700 4300 2700
Wire Wire Line
	4000 2700 4000 2300
Connection ~ 4000 2300
Wire Wire Line
	4450 2600 3750 2600
Wire Wire Line
	3750 2600 3750 2300
Wire Wire Line
	3750 2000 3750 1700
Connection ~ 3750 1700
Wire Wire Line
	3750 1700 4950 1700
$Comp
L Device:C C3
U 1 1 5DD6AEE8
P 3700 2950
F 0 "C3" V 3952 2950 50  0000 C CNN
F 1 "15n" V 3861 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3738 2800 50  0001 C CNN
F 3 "~" H 3700 2950 50  0001 C CNN
F 4 "CGA4F2C0G1H153J085AA" H 3700 2950 50  0001 C CNN "MFP"
F 5 "TDK" H 3700 2950 50  0001 C CNN "MFN"
	1    3700 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3300 2950 3550 2950
Wire Wire Line
	3850 2950 4200 2950
Wire Wire Line
	4200 2950 4200 2800
Wire Wire Line
	4200 2800 4450 2800
$Comp
L Device:C C5
U 1 1 5DD760D5
P 5700 2000
F 0 "C5" V 5448 2000 50  0000 C CNN
F 1 "2.2u" V 5539 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5738 1850 50  0001 C CNN
F 3 "~" H 5700 2000 50  0001 C CNN
F 4 "EMK212BJ225KG-T" H 5700 2000 50  0001 C CNN "MFP"
F 5 "Taiyo Yuden" H 5700 2000 50  0001 C CNN "MFN"
	1    5700 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2000 5550 2000
Wire Wire Line
	5850 2000 6050 2000
Text Label 6050 2000 2    50   ~ 0
GND_D
$Comp
L Transistor_FET:CSD19534Q5A Q2
U 1 1 5DD7D8A6
P 6350 3250
F 0 "Q2" H 6556 3296 50  0000 L CNN
F 1 "CSD19534Q5A" H 6556 3205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TDSON-8-1" H 6550 3175 50  0001 L CIN
F 3 "http://www.ti.com/lit/gpn/csd19534q5a" V 6350 3250 50  0001 L CNN
F 4 "CSD19534Q5A" H 6350 3250 50  0001 C CNN "MFP"
F 5 "Texas Instruments" H 6350 3250 50  0001 C CNN "MFN"
	1    6350 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4000 7350 4000
Wire Wire Line
	6950 2800 7350 2800
Wire Wire Line
	6450 3050 6450 2800
Wire Wire Line
	6150 3250 6050 3250
Wire Wire Line
	6050 3250 6050 2500
Wire Wire Line
	6050 2500 5850 2500
Wire Wire Line
	5350 3650 6450 3650
Wire Wire Line
	6450 3650 6450 3450
Wire Wire Line
	8050 3800 8450 3800
Wire Wire Line
	7700 3800 8050 3800
Connection ~ 8050 3800
Wire Wire Line
	8050 3450 8050 3800
$Comp
L Device:C C7
U 1 1 5DD2E9E2
P 8050 3300
F 0 "C7" H 8165 3346 50  0000 L CNN
F 1 "22u" H 8165 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_2220_5650Metric_Pad1.97x5.40mm_HandSolder" H 8088 3150 50  0001 C CNN
F 3 "~" H 8050 3300 50  0001 C CNN
F 4 "CKG57NX5R1H226M500JH" H 8050 3300 50  0001 C CNN "MFP"
F 5 "TDK" H 8050 3300 50  0001 C CNN "MFN"
	1    8050 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2800 8450 2800
Wire Wire Line
	7700 2800 8050 2800
Connection ~ 8050 2800
Wire Wire Line
	8050 2800 8050 3150
$Comp
L Device:C C8
U 1 1 5DDD3439
P 8450 3300
F 0 "C8" H 8565 3346 50  0000 L CNN
F 1 "22u" H 8565 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_2220_5650Metric_Pad1.97x5.40mm_HandSolder" H 8488 3150 50  0001 C CNN
F 3 "~" H 8450 3300 50  0001 C CNN
F 4 "CKG57NX5R1H226M500JH" H 8450 3300 50  0001 C CNN "MFP"
F 5 "TDK" H 8450 3300 50  0001 C CNN "MFN"
	1    8450 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5DDD3C81
P 8800 3300
F 0 "C9" H 8915 3346 50  0000 L CNN
F 1 "22u" H 8915 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_2220_5650Metric_Pad1.97x5.40mm_HandSolder" H 8838 3150 50  0001 C CNN
F 3 "~" H 8800 3300 50  0001 C CNN
F 4 "CKG57NX5R1H226M500JH" H 8800 3300 50  0001 C CNN "MFP"
F 5 "TDK" H 8800 3300 50  0001 C CNN "MFN"
	1    8800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3150 8450 2800
Connection ~ 8450 2800
Wire Wire Line
	8450 2800 8800 2800
Wire Wire Line
	8450 3450 8450 3800
Connection ~ 8450 3800
Wire Wire Line
	8450 3800 8800 3800
Wire Wire Line
	8800 3150 8800 2800
Connection ~ 8800 2800
Wire Wire Line
	8800 2800 9250 2800
Wire Wire Line
	8800 3450 8800 3800
Connection ~ 8800 3800
Wire Wire Line
	8800 3800 9250 3800
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DDE466B
P 6450 1700
F 0 "#FLG0101" H 6450 1775 50  0001 C CNN
F 1 "PWR_FLAG" H 6450 1873 50  0000 C CNN
F 2 "" H 6450 1700 50  0001 C CNN
F 3 "~" H 6450 1700 50  0001 C CNN
	1    6450 1700
	1    0    0    -1  
$EndComp
Connection ~ 6450 1700
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DDE7E04
P 5350 2000
F 0 "#FLG0102" H 5350 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 5350 2173 50  0000 C CNN
F 2 "" H 5350 2000 50  0001 C CNN
F 3 "~" H 5350 2000 50  0001 C CNN
	1    5350 2000
	1    0    0    -1  
$EndComp
Connection ~ 5350 2000
$Comp
L Device:C C10
U 1 1 5DD41D31
P 2500 2450
F 0 "C10" H 2615 2496 50  0000 L CNN
F 1 "4.7u" H 2615 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2538 2300 50  0001 C CNN
F 3 "~" H 2500 2450 50  0001 C CNN
F 4 "C2012X5R1H475K125AB" H 2500 2450 50  0001 C CNN "MFP"
F 5 "TDK" H 2500 2450 50  0001 C CNN "MFN"
	1    2500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2300 2500 1700
Connection ~ 2500 1700
Wire Wire Line
	2500 1700 1750 1700
Wire Wire Line
	2500 2600 2500 2950
Wire Wire Line
	2500 2950 2900 2950
Connection ~ 2900 2950
$EndSCHEMATC
